import _ from 'lodash';
import React from 'react';
import TodosListItem from './todos-list-item';

export default class TodoList extends React.Component {
    renderItems() {
        const props = _.omit(this.props, 'todos');

        return _.map(this.props.todos, (todo, index) => <TodosListItem id={index} key={index} {...todo} {...this.props}/>)
    }

    render() {
        return (
            <div className="todo-block__list">
                {this.renderItems()}
            </div>
        )
    }
}