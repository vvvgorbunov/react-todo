import _ from 'lodash';
import React from 'react';

export default class TodosListItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isEditing: false
        }
    }

    renderTaskSection() {
        const {task, isCompleted} = this.props;

        if (this.state.isEditing) {
            return (
                <form onSubmit={this.onSaveClick.bind(this)}>
                    <input className="input-edit" type="text" defaultValue={task} ref="editInput"/>
                </form>
            );
        }

        return (
            <div className="checkbox-container">
                <input type="checkbox" defaultChecked={isCompleted}  id={this.props.id} className="checkbox-input"/>
                <label onClick={this.props.toggleTask.bind(this, task)} className="checkbox-label" htmlFor={this.props.id}>{this.props.task}</label>
            </div>
        );
    }

    renderActionsSections() {
        if (this.state.isEditing) {
            return (
                <div className="row-action">
                    <button onClick={this.onSaveClick.bind(this)} className="row-save">
                        <i className="fa fa-check-square" aria-hidden="true"></i>
                    </button>
                    <button onClick={this.onCancelClick.bind(this)} className="row-cancel">
                        <i className="fa fa-caret-square-o-right" aria-hidden="true"></i>
                    </button>
                </div>
            );
        }

        return (
            <div className="row-action">
                <button onClick={this.onEditClick.bind(this)} className="row-edit">
                    <i className="fa fa-pencil-square" aria-hidden="true"></i>
                </button>
                <button onClick={this.props.deleteTask.bind(this, this.props.task)} className="row-del">
                    <i className="fa fa-minus-square" aria-hidden="true"></i>
                </button>
            </div>
        );
    }

    render() {
        return (
            <div className="todo-block__list_row">
                {this.renderTaskSection()}
                {this.renderActionsSections()}
            </div>
        )
    }

    onEditClick() {
        this.setState({isEditing: true});
    }

    onCancelClick() {
        this.setState({isEditing: false});
    }

    onSaveClick(event) {
        event.preventDefault();

        const oldTask = this.props.task;
        const newTask = this.refs.editInput.value;
        this.props.saveTask(oldTask,newTask);
        this.setState({isEditing: false});
    }
}