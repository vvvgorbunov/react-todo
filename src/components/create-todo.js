import _ from 'lodash';
import React from 'react';

export default class CreateTodo extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            error: null
        }
    }

    renderError() {
        if (!this.state.error) {
            return null;
        }

        return (
            <span className="error-message">{this.state.error}</span>
        );
    }


    render() {
        return (
            <div className="todo-block__create">
                <form onSubmit={this.handleCreate.bind(this)}>
                    <input type="text" placeholder="что делаем?" ref="createInput"/>
                    <button>
                        <i className="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </form>
                {this.renderError()}
            </div>
        )
    }

    handleCreate(even) {
        even.preventDefault();

        const createInput = this.refs.createInput;
        const task = createInput.value;
        const validateInput = this.validateInput(task);

        if (validateInput) {
            this.setState({error: validateInput});

            return;
        }

        this.setState({error: null});
        this.props.createTask(task);
        this.refs.createInput.value = '';
    }

    validateInput(task) {
        if (!task) {
            return 'Введите задачу';
        } else if (_.find(this.props.todos, todo => todo.task === task)) {
            return 'Такая задача уже есть!';
        } else {
            return null;
        }
    }
}